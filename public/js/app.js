$(document).ready(function() {
	
	svg4everybody(); // To work with SVG icons - As explained in https://fvsch.com/code/svg-icons/how-to/

	//When scrolling the page
	$(window).bind('scroll', function(e) {

	});

	//When changing the window size
	$(window).resize(function() {

	});

	//When changing the device orientation
	$(window).on("orientationchange", function() {

	});

});