# Ingresso Certo Progressive Web Application #

Ingressocerto is the major event ticket sales service in Rio de Janeiro and this is the company first attempt to convert their site and mobile apps to a single multi platform application.

## Installation

  Setup your machine with Node, Grunt-cli and Sass.

#### - Install/Update [Node.js](https://nodejs.org/) v4+
Node.js (Node) is an open source development platform for executing JavaScript code server-side. Node.js package ecosystem, npm, is the largest ecosystem of open source libraries in the world.
```sh
$ npm update -g npm
```

#### - Install [grunt-cli](https://gruntjs.com/getting-started#installing-the-cli)
This will install Grunt command-line interface that comes with a series of options.
```sh
$ npm install -g grunt-cli
```

#### - Install [Sass](http://sass-lang.com/install)
Sass is an extension of CSS that adds power and elegance to the basic language. It allows you to use variables, nested rules, mixins, inline imports, and more, all with a fully CSS-compatible syntax. Sass helps keep large stylesheets well-organized, and get small stylesheets up and running quickly.
```sh
$ sudo gem install sass
$ sudo gem update sass
```

Now go to the directory where you want the project to be and follow the steps below.

#### - Clone this repository
```sh
$ git clone git@bitbucket.org:felipepvc/ic_pwa.git
```

#### - Download dependencies
Go one path deeper `$ cd ic_pwa`, to the repository's root directory where the file package.json is. It will install all the dependencies listed on package.json, including bootstrap-sass.
```sh
$ npm install
```

#### - Run Grunt
```sh
$ grunt
```
It will open a new window on your browser, and self update the page everytime you modify and save the html or scss file. It will also compile scss files to css, merge the svg images into a single svg sprite image and other things... 

All the details about the automations made by grunt are listed in Gruntfile.js.


## Contributing

1. Fork it
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -am 'Add some feature')
4. Push to the branch (git push origin my-new-feature)
5. Create new Pull Request