module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    sass: {
      dist: {
        options: {                       // Target options
          style: 'compressed',
          noCache: true,
          sourcemap: 'none'
        },
        files: [{
          expand: true,
          cwd: 'src/scss/',
          src: ['*.scss'],
          dest: 'public/css/',
          ext: '.min.css'
        }]
      }
    },
    watch: {
      gruntfile: {
        files: 'Gruntfile.js',
        tasks: ['default'],
      },
      css: {
        files: ['src/scss/**'],
        tasks: ['sass', 'postcss'],
      },
      svgs: {
        files: ['src/svgs/**'],
        tasks: ['svgstore', 'svg2png'],
      },
    },
    postcss: {
      options: {
        processors: [
          require('autoprefixer')({browsers: 'last 5 versions'}), // add vendor prefixes
          require('cssnano')({ // minify the result
            preset: ['default', {
                discardComments: {
                    removeAll: true,
                },
            }]
          })
        ]
      },
      dist: {
        src: 'public/css/*.css'
      }
    },
    browserSync: {
      dev: {
          bsFiles: {
              src : './public/**/*'
          },
          options: {
              watchTask: true,
              server: './public'
          }
      }
    },
    svgstore: {
      options: {
        cleanup: true,
        cleanupdefs: true,
        includeTitleElement: false,
        preserveDescElement: false,
        convertNameToId: function(name) {
            var dotPos = name.indexOf('.');
            if ( dotPos > -1){
              name = name.substring(0, dotPos);
            }
            return name;
          }
      },
      default : {
        files: {
          './public/imgs/icons.svg': ['./src/svgs/**/*.svg'],
        },
      },
    },
    svg2png: {
      build: {
        files: [{
          cwd: './src/svgs/',
          src: ['**/*.svg'],
          dest: './public/imgs/'
        }]
      }
    }
  });

  // Load the plugins that provides the tasks.
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-svgstore');
  grunt.loadNpmTasks('grunt-svg2png');
  grunt.loadNpmTasks('grunt-browser-sync');

  // Default task(s).
  grunt.registerTask('default', ['sass', 'postcss', 'svgstore', 'svg2png', 'browserSync', 'watch']);

};